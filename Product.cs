using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ImportProducts.Models.Sezoone
{
        [Table("Products")]
    public class Product
    {
        public int Id { get; set; }
        public string IdProd { get; set; }
        public string Name { get; set; }
        public Nullable<decimal> Prise { get; set; }
        public string Currency { get; set; }
        public string Image { get; set; }
    }
}