using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ImportProducts.Models;
using ImportProducts.Models.Sezoone;
using System.Data.Entity;
using System.Windows;
using System.Windows.Forms;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Xml;

namespace ImportProducts.Controllers
{
    public class HomeController : Controller
    {
        SezooneContext db = new SezooneContext();
        string userPath = "/Products/";

        [HttpGet]
        public ActionResult VProducts()
        {
            ViewBag.xmlPath = new SelectList(db.FOLDERS.OrderBy(n => n.Folder), "Folder", "Folder");
            ViewBag.jsonPath = new SelectList(db.FOLDERS.OrderBy(n => n.Folder), "Folder", "Folder");
            var listProd = (from pd in db.PRODUCTS
                            select pd).ToList();
            return View(listProd);
        }

        [HttpGet]
        public ActionResult EditVProducts(int id = 0)
        {
            ViewBag.myPath = new SelectList(db.FOLDERS.OrderBy(n => n.Folder), "Folder", "Folder");
            return View(db.PRODUCTS.Find(id));
        }

        [HttpPost]
        public ActionResult EditVProducts(Product prod, string fileImage)
        {

            HttpPostedFileBase mFile = Request.Files["myFile"];
            string fName = Path.GetFileName(mFile.FileName);

            string mPath = Request["myPath"];

            //MessageBox.Show(userPath+mPath+"/"+fName+"fileImage="+fileImage);
            if (mPath != null)
            {
                if (fName != "")
                {
                    prod.Image = userPath + mPath + "/" + fName;
                    mFile.SaveAs(Path.Combine(Server.MapPath(userPath + mPath + "/"), fName));
                }
                else
                {
                    prod.Image = fileImage;

                }
            }
            else
            {
                if (fName != "")
                {
                    prod.Image = userPath + fName;
                    mFile.SaveAs(Path.Combine(Server.MapPath(userPath), fName));
                }
                else
                {
                    prod.Image = fileImage;

                }
            }
            db.Entry(prod).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("VProducts");
        }



        [HttpGet]
        public ActionResult CreateVProducts()
        {
            ViewBag.myPath = new SelectList(db.FOLDERS.OrderBy(n => n.Folder), "Folder", "Folder");
            return View();
        }


        [HttpPost]
        public ActionResult CreateVProducts(Product prod)
        {

            HttpPostedFileBase mFile = Request.Files["myFile"];
            string fName = Path.GetFileName(mFile.FileName);

            string mPath = Request["myPath"];

            // MessageBox.Show(userPath+mPath+"/"+fName);
            if (mPath != null)
            {
                prod.Image = userPath + mPath + "/" + fName;
                mFile.SaveAs(Path.Combine(Server.MapPath(userPath + mPath + "/"), fName));
            }
            else
            {
                prod.Image = userPath + fName;
                mFile.SaveAs(Path.Combine(Server.MapPath(userPath), fName));
            }
            using (db)
            {
                db.PRODUCTS.Add(prod);
                db.SaveChanges();
            }
            return RedirectToAction("VProducts");
        }

        [HttpGet]
        public ActionResult DeleteVProducts(int id = 0)
        {
            return View(db.PRODUCTS.Find(id));
        }

        [HttpPost, ActionName("DeleteVProducts")]
        public ActionResult DeleteVProductsPost(int id)
        {
            Product myProduct = db.PRODUCTS.Find(id);
            db.PRODUCTS.Remove(myProduct);
            db.SaveChanges();
            return RedirectToAction("VProducts");
        }



        [HttpGet]
        public ActionResult DetailVProducts(int id = 0)
        {
            return View(db.PRODUCTS.Find(id));
        }

        [HttpPost]
        public ActionResult DetailVProducts()
        {
            return RedirectToAction("VProducts");
        }



        //export to XML
        public ActionResult ExportXML()
        {
            //string fileName = "D:\\VSSolution\\SeezonaImportXML\\ImportProducts\\Data.xml";

            SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["SezooneContext"].ConnectionString);
            con.Open();
            string query = "SELECT IdProd, Name, Prise, Currency, Image FROM [Products]";
            SqlCommand cmd = new SqlCommand(query, con);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            ds.Tables[0].WriteXml(@"D:\\VSSolution\\SeezonaImportXML\\ImportProducts\\Export\\Data.xml");
            con.Close();

            MessageBox.Show("File was exported to XML!");
            return RedirectToAction("VProducts");
        }



        //import to table  Products
      public ActionResult ImportXML()
        {
            
            //take the path
           
            HttpPostedFileBase mFile = Request.Files["xmlFile"];
            string fName = Path.GetFileName(mFile.FileName);
            //MessageBox.Show(fName);
             //copy file to the import folder 
            if (fName != "")
            {
                mFile.SaveAs(Path.Combine(Server.MapPath("/Import/"), fName));

                SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["SezooneContext"].ConnectionString);
                con.Open();

                SqlCommand cmd;
                SqlDataAdapter da = new SqlDataAdapter();
                DataSet ds = new DataSet();
                XmlReader xmlFile;

                string fileName = "D:\\VSSolution\\SeezonaImportXML\\ImportProducts\\Import\\"+fName;
                //MessageBox.Show("'"+fileName+"'");
                xmlFile = XmlReader.Create(@fileName, new XmlReaderSettings());
                ds.ReadXml(xmlFile);

                int i = 0;
                string IdProd = null;
                string Name = null;
                double Prise = 0;
                string Currency = null;
                string Image = null;
                string sql = null;
            
                for (i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    IdProd = ds.Tables[0].Rows[i].ItemArray[0].ToString();
                    Name = ds.Tables[0].Rows[i].ItemArray[1].ToString();
                    Prise = Convert.ToDouble(ds.Tables[0].Rows[i].ItemArray[2]);
                    Currency = ds.Tables[0].Rows[i].ItemArray[3].ToString();
                    Image = ds.Tables[0].Rows[i].ItemArray[4].ToString();
                    sql = "insert into Products(IdProd, Name, Prise, Currency, Image) values(" + "'" + IdProd + "','" + Name + "'," + Prise + ",'" + Currency + "','" + Image + "'" + ")";
                    //MessageBox.Show(sql);
                    cmd = new SqlCommand(sql, con);
                    da.InsertCommand = cmd;
                    da.InsertCommand.ExecuteNonQuery();
                }
                con.Close();

                MessageBox.Show("File was imported to Products!");
            }
            else
            {
                MessageBox.Show("Please,choose the file!");
            }//end if


            return RedirectToAction("VProducts");
        }


        //import JSON format
        public ActionResult ImportJSON()
        {
            return RedirectToAction("VProducts");
        }



        //export JSON format file
        public ActionResult ExportJSON()
        {
            //string fileName = "D:\\VSSolution\\SeezonaImportXML\\ImportProducts\\Data.xml";

            SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["SezooneContext"].ConnectionString);
            con.Open();
            string query = "SELECT IdProd, Name, Prise, Currency, Image FROM [Products]";
            SqlCommand cmd = new SqlCommand(query, con);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            ds.Tables[0].WriteXml(@"D:\\VSSolution\\SeezonaImportXML\\ImportProducts\\Export\\Data.xml");
            con.Close();

            MessageBox.Show("File was exported to XML!");
            return RedirectToAction("VProducts");
        }
    }
}